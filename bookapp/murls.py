from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView


from bookapp.views import list_magazines, create_magazine, edit_magazine, magazine_detail, delete_magazine, list_issue, list_genre_str, list_genre

urlpatterns = [
    path("", list_magazines, name="list_magazines"),
    path("mcreate", create_magazine, name="create_magazine"),
    path("<int:pk>/medit", edit_magazine, name="edit_magazine"),
    path("<int:pk>/issues", list_issue, name="list_issue"),
    path("<int:pk>/delete", delete_magazine, name="delete_magazine"),
    path("<int:pk>/magdetail", magazine_detail, name="magazine_detail"),
    path("<str:magazine_genre>/", list_genre_str, name="list_genre"),
    path("mgenres/<int:pk>/", list_genre, name="list_genre")
]