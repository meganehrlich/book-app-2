from django.shortcuts import render, redirect, get_object_or_404
from bookapp.models import Book, Magazine, Author, Issue, Genre
from bookapp.forms import BookForm, MagazineForm

# Create your views here.
# import Book
#queryset mothod book.objects.all

def list_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)


def create_book(request):
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("list_books")

    context["form"]=form
    return render(request, "books/create.html", context)

def book_detail(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
        
    }
    return render(request, "books/detail.html", context)


def edit_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book=form.save()
        return redirect (book_detail, pk=book.pk)
    context['form'] = form
    return render(request, "books/edit.html", context)


def delete_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)

    if request.method =="POST":
        obj.delete()
        return redirect (list_books)

    return render(request, "books/delete.html", context)
    

# Magazines Section
def list_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines": magazines
    }
    return render(request, "magazines/mlist.html", context)

def create_magazine(request):
    context = {}
    form = MagazineForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect(list_magazines)
    context["form"]=form
    return render(request, "magazines/mcreate.html", context)



def edit_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect(magazine_detail, pk=pk)
    context['form'] = form
    return render(request,"magazines/medit.html", context)

def magazine_detail(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,

    }
    return render(request, "magazines/magdetail.html", context)


def delete_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)

    if request.method =="POST":
        obj.delete()
        return redirect(list_magazines)
    
    return render(request, "magazines/mdelete.html", context)

def list_issue(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine": magazine
    }
    return render(request, "magazines/issues.html", context)

def list_genre_str(request, genre_name):
    genre = Genre.objects.get(name=genre_name.capitalize())
    magazines = genre.magazines.all()

    context = {
        "magazines": magazines
    }
    return render(request, "magazines/mgenre.html", context)

def list_genre(request, pk):
    genre = Genre.objects.get(pk=pk)
    magazines = genre.magazines.all()
    context = {
        "magazines" : magazines
    }
    return render (request, "magazines/mgenres.html", context)
    