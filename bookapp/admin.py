from django.contrib import admin
from bookapp.models import Book, BookReview, Magazine, Author, Issue, Genre

# Register your models here.


admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Issue)
admin.site.register(Genre)