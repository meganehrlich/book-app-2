from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView
from bookapp.views import create_book, list_books, book_detail, edit_book, delete_view, list_magazines
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("", list_books, name="list_books"),
    path("create/", create_book, name="create_books"),
    path("<int:pk>/", book_detail, name="book_detail"),
    path("<int:pk>/edit/", edit_book, name="edit_book"),
    path("<int:pk>/delete/", delete_view, name="delete_view"),
    
    
]
