# Generated by Django 4.0.6 on 2022-07-21 17:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookapp', '0016_alter_genre_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='magazine',
            name='genre',
        ),
        migrations.DeleteModel(
            name='Genre',
        ),
    ]
