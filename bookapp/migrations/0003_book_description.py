# Generated by Django 4.0.6 on 2022-07-19 19:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookapp', '0002_book_in_print'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='description',
            field=models.CharField(default='Help', max_length=500),
        ),
    ]
