# Generated by Django 4.0.6 on 2022-07-21 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookapp', '0015_remove_genre_genre_description_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='genre',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
