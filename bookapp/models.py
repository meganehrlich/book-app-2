from django.db import models
from django.forms import ModelForm

# class Author(models.Model):
#     name = models.CharField(max_length=200, unique=True)
#     def __str__(self):
#         return self.name

class Book(models.Model):
    title = models.CharField(max_length=120, unique=True)
    author = models.ManyToManyField("Author", related_name="book")
    isbn = models.BigIntegerField(null=True, blank=True)
    pages = models.IntegerField(null=True, blank=True)
    publish = models.SmallIntegerField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True, blank=True)
    description = models.TextField()
    def __str__(self):
        return self.title + " by " + self.author


class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

class Magazine(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    release_cycle = models.CharField(max_length=50)
    cover = models.URLField(null=True, blank=True)
    genre = models.ForeignKey("Genre", on_delete=models.CASCADE, blank=True, null=True, related_name="magazines")
    def __str__(self):
        return self.title
        
class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.name

class Issue(models.Model):
    magazine_title = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    issue_title = models.CharField(max_length=100)
    description = models.TextField()
    cover = models.URLField(null=True, blank=True)
    date_published = models.CharField(max_length=100, blank=True)
    issue_number = models.CharField(max_length=100, blank=True)
    page_count = models.CharField(max_length=100, blank=True)
    def str__(self):
        return self.issue_title

class Genre(models.Model):  
    magazine_genre = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.magazine_genre